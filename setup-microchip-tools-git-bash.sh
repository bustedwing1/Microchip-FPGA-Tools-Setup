
#===============================================================================
#
# Microchip FPGA Tools Setup for Git Bash (Windows)
# Assumes a node-locked license
#
# Example:
#   setup-microchip-tools-git-bash.sh
#
# Edit the following section with the location where the following tools are
# installed:
#   - SoftConsole (SC_INSTALL_DIR)
#   - Libero (LIBERO_INSTALL_DIR)
#   - Licensing daemon for Libero (LICENSE_DAEMON_DIR)
#===============================================================================

export SC_INSTALL_DIR=/c/Microchip/SoftConsole-v2022.2-RISC-V-747
export LIBERO_INSTALL_DIR=/c/Microchip/Libero_SoC_v2023.2
##export LICENSE_DAEMON_DIR=/home/$USER/Microchip/Linux_Licensing_Daemon
export LICENSE_FILE_DIR=/c/flexlm/License.dat

#===============================================================================
# The following was tested on Ubuntu 20.04 with:
#   - Libero 2023.2
#   - SoftConsole 2022.2
#===============================================================================

#
# SoftConsole
#
export PATH=$PATH:$SC_INSTALL_DIR/riscv-unknown-elf-gcc/bin
export FPGENPROG=$LIBERO_INSTALL_DIR/Designer/bin64/fpgenprog.exe

#
# Libero
#
export PATH=$PATH:$LIBERO_INSTALL_DIR/Designer/bin:$LIBERO_INSTALL_DIR/Designer/bin64
export PATH=$PATH:$LIBERO_INSTALL_DIR/SynplifyPro/bin
export PATH=$PATH:$LIBERO_INSTALL_DIR/Model/modeltech/linuxacoem
export SITE_PACKAGES=/c/Users/bustedwing/AppData/Local/Packages/PythonSoftwareFoundation.Python.3.12_qbz5n2kfra8p0/LocalCache/local-packages/Python312/site-packages
export PATH=$PATH:$SITE_PACKAGES

export LOCALE=C
###export LD_LIBRARY_PATH=/usr/lib/i386-linux-gnu:$LD_LIBRARY_PATH

#
# Libero License daemon
#
###export LM_LICENSE_FILE=1702@localhost
###export SNPSLMD_LICENSE_FILE=1702@localhost

###$LICENSE_DAEMON_DIR/lmgrd -c $LICENSE_FILE_DIR/License.dat -l $LICENSE_FILE_DIR/license.log

alias softconsole=/c/microchip/SoftConsole-v2022.2-RISC-V-747/softconsole.cmd

